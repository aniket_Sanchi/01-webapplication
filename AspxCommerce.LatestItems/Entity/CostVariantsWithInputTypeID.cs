﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SanchiCommerce.LatestItems
{
    public class CostVariantsWithInputTypeID
    {
        public CostVariantsWithInputTypeID() { }
        public SanchiCommerce.Core.CostVariants CostVariant { get; set; }
        public int InputTypeID { get; set; }
    }
}
