﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SageFrame.Web;

public partial class Modules_SanchiCommerce_AspxItemSocialLinks_ItemSocialLinks :BaseUserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        IncludeCss("ItemSocialLinks", "/Modules/SanchiCommerce/AspxItemSocialLinks/css/smpl-share.css");
        IncludeJs("ItemSocialLinks","/Modules/SanchiCommerce/AspxItemSocialLinks/Js/smpl-share.js");
    }
}