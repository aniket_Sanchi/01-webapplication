<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LoginStatus.ascx.cs" Inherits="Modules_Admin_LoginControl_LoginStatus"
    EnableViewState="false" %>
<%@ Register Src="~/Controls/LoginStatus.ascx" TagName="LoginStatus" TagPrefix="uc1" %>
<script type="text/javascript">
    $(function () {
        $(".sfLocalee").SystemLocalize();
    });
</script>
<style>
#IdWelcome ul {float:none!important}
#IdWelcome ul.right li.logout {border-left: none;position: relative;cursor: pointer;padding: 0;    margin: 0;}
#IdWelcome ul.right li.loggedin {position: relative;padding-right: 28px;text-transform:capitalize;float: left;margin-right: 0;color: #666;}
#IdWelcome .icon-user:before {top:1px!important}
#IdWelcome ul.right li div.myProfileDrop ul li {display: block;width: 100%;border-left: none;float: none;width: auto;padding: 0px;height: auto;line-height: normal;color: #666;margin-right: 0;}
.myProfileDrop {min-width: 105px;top: 30px;}
#IdWelcome ul.right li div.myProfileDrop ul li a:hover{background: #0088cc;color: #fff;text-decoration: none;}
#IdWelcome ul.right li div.myProfileDrop ul li a {text-align: right;float: none;padding: 5px 10px;color: #1a1a1a;display: block;line-height: normal;height: auto;}
.icon-arrow-s:before {font-size: 8px;}
.myProfile {padding-right:0px}


</style>
<div class="sfLogininfo">
    <ul>
        <div id="LoginView1" runat="server" enableviewstate="False">
            <div id="divAnonymousTemplate" runat="server">
                <li class="sfLogin">
                    <uc1:LoginStatus ID="LoginStatus1" runat="server" EnableViewState="False" />
                </li>
                 
                
                <li class="sfLoginCredInfo" style="display:none">
                    <a href="#">Backend Login info</a>
                    <div class="sfLoginCredInfoBox">
                        <div class="triangle"></div>
                        <h6>Login credentials for backend</h6>
                        <div class="sfSuperUserInfo">
                            <p><span>Username: </span>superuser</p>
                            <p><span>Password: </span>superuser</p>
                        </div>
                        <div class="sfAdminInfo">
                            <p><span>Username: </span>admin</p>
                            <p><span>Password: </span>admin</p>
                        </div>
                    </div>

                </li>
                <li class="sfRegister">
                    <%=RegisterURL%>
                </li>
               <%-- <li class="sfLogin">
                    <p>Cusromer Care: +91-8252 101 101</p>
                </li>--%>
            </div>
            <div id="divLoggedInTemplate" runat="server">
                <ul id="IdWelcome" class="IdWelcome">
                <li class="sfWelcomeMsg">
                    <asp:Label ID="lblWelcomeMsg" Text="Welcome" runat="server" meta:resourcekey="lblWelcomeMsgResource1"></asp:Label>
                </li>
              <%-- <li class="sfWelcomeMsg">
                    <asp:Label ID="lblWelcomeMsgSignIN" runat="server" meta:resourcekey="lblWelcomeMsgSignINResource1"></asp:Label>
                </li>
                <li>
                    <asp:Label ID="lblProfileURL" runat="server" meta:resourcekey="lblProfileURLResource1"></asp:Label>
                </li>
                <li class="sfLoggedOut">
                    <uc1:LoginStatus ID="LoginStatus2" runat="server" EnableViewState="False" />
                </li>--%>
                </ul>
            </div>
        </div>
    </ul>
</div>
