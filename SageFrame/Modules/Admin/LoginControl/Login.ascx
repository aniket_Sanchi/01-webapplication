<%@ Control Language="C#" AutoEventWireup="true" CodeFile="Login.ascx.cs" Inherits="SageFrame.Modules.Admin.LoginControl.Login" %>
<%@ Register Src="../../../Controls/LoginStatus.ascx" TagName="LoginStatus" TagPrefix="uc1" %>

<script type="text/javascript">
    //<![CDATA[  
    var elementId = '#<%=UserName.ClientID%>';
    $(function() {
        $(".sfLocale").SystemLocalize();
    });
      var loginbut = '#' + '<%=LoginButton.ClientID%>';
    $(document).ready(function () {
        validate();
        $('input').on('keyup', validate);
        $(loginbut).click(function () { $(".sfBtn").addClass("loadingClass"); })
    });
    function validate() {
        var codeValue = $("#<%= UserName.ClientID %>").val();
        var nameValue = $("#<%= Password.ClientID %>").val();
        var captcha = $("#<%=CaptchaValue.ClientID%>").val();
        if ($("#<%= dvCaptchaField.ClientID %>").is(':visible')) {
            if (codeValue == "" || nameValue == "" || captcha=="") {
                $("input[type=submit]").prop("disabled", true);
                $(".sfBtn").addClass("notallowed");
            } else {
                $("input[type=submit]").prop("disabled", false);
                $(".sfBtn").removeClass("notallowed");
            }
            return;
        }
        else {
            if (codeValue == "" || nameValue == "") {
                $("input[type=submit]").prop("disabled", true);
                $(".sfBtn").addClass("notallowed");
            } else {
                $("input[type=submit]").prop("disabled", false);
                $(".sfBtn").removeClass("notallowed");
            }
            return;
        }
    }
  
    //]]>
</script>
<asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
    <asp:View ID="View1" runat="server">
        <div class="sfLogin">
            <div class="sfLogininside">
                <h2>
                    <asp:Label ID="lblAdminLogin" runat="server" Text="Login" meta:resourcekey="lblAdminLoginResource1"></asp:Label>
                </h2>
                <p class="sfUserName">
                    <asp:TextBox ID="UserName" placeholder="Email / Mobile number" runat="server"  meta:resourcekey="UserNameResource1" autofocus="autofocus"
                        CssClass="sfInputbox" ToolTip="Enter email or mobile number." ></asp:TextBox>
                    <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                        ErrorMessage="Username is required." ValidationGroup="Login1"
                        CssClass="sfError" meta:resourcekey="UserNameRequiredResource1" InitialValue=""
                        Text="Enter email or mobile number."></asp:RequiredFieldValidator>
                </p>
                <p class="sfPassword">
                    <asp:TextBox ID="Password" placeholder="Password" runat="server" Maxlength="20" TextMode="Password"
                        meta:resourcekey="PasswordResource1" CssClass="sfInputbox" ></asp:TextBox>
                    <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                        ErrorMessage="Password is required." ToolTip="Password is required." ValidationGroup="Login1"
                        CssClass="sfError" meta:resourcekey="PasswordRequiredResource1" Text="Password is required."></asp:RequiredFieldValidator>
                </p>
                <p>
                    <asp:CheckBox ID="chkRememberMe" runat="server" CssClass="sfCheckBox" meta:resourcekey="RememberMeResource1" />
                    <asp:Label ID="lblrmnt" runat="server" Text="Remember me." CssClass="sfFormlabel"
                        meta:resourcekey="lblrmntResource1"></asp:Label>
                </p>
                <div id="dvCaptchaField" runat="server" style="clear: both;">
                    <p class="sfCaptchaImage" style="margin-bottom:20px;">
                      
                        <asp:Image ID="CaptchaImage" runat="server" CssClass="sfCaptcha" meta:resourcekey="CaptchaImageResource1" />
                    <%--    <span id="captchaValidator" runat="server" class="sfError">*</span>--%>
                               
                        <asp:ImageButton ID="Refresh" CssClass="sfCaptchadata" runat="server" ValidationGroup="Sep"
                            OnClick="Refresh_Click" meta:resourcekey="RefreshResource1" />
                          
                    </p>
                    <p>
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <%--<p class="sfLocale" style="clear:both; white-space:nowrap;">Enter Captcha Text</p>--%>
                                <p class="sfCaptcha" style="margin-top:50px;">
                                    <asp:TextBox placeholder="Enter captcha text" ID="CaptchaValue" runat="server" CssClass="sfInputbox" meta:resourcekey="CaptchaValueResource1" MaxLength="6"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvCaptchaValueValidator" runat="server" ControlToValidate="CaptchaValue"
                                        Display="Dynamic" ErrorMessage="*" ValidationGroup="Login1" CssClass="sfErrorA"
                                        meta:resourcekey="rfvCaptchaValueValidatorResource1"></asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="cvCaptchaValue" runat="server" Display="Dynamic" ErrorMessage="*"
                                        ControlToValidate="CaptchaValue" ValueToCompare="121" CssClass="sfError" meta:resourcekey="cvCaptchaValueResource1"></asp:CompareValidator>
                                </p>
                                 <p class="sfError"> You have 2 more attempts before your account is locked . If you run out of attempts please reset your password by clicking the below link.</p>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </p>
                </div>
                <p style="height:30px;">
                    <span class="cssClassForgotPass">
                       
                        <asp:HyperLink ID="hypForgotPassword" runat="server" meta:resourcekey="hypForgotPasswordResource1"
                            Text="Forgot Password?"></asp:HyperLink>
                    </span>
                </p>
                <div class="sfButtonwrapper" >
                    <span><span>
                        <asp:Button ID="LoginButton" runat="server" CommandName="Login" CssClass="sfBtn"
                            meta:resourcekey="LoginButtonResource1" OnClick="LoginButton_Click" Text="Login"
                            ValidationGroup="Login1" Enabled="true" disabled="true"/>
                    </span></span>
                </div>
                
                <p style="clear: both;">
                    <asp:Literal ID="FailureText" runat="server" EnableViewState="False" meta:resourcekey="FailureTextResource1"></asp:Literal>
                </p>
            </div>
             <div class="OpenID" align="center" runat="server" id="divOpenIDProvider" style="display:none">
            <h3 class="sfLocale">
                Login with facebook
            </h3>
            <asp:ImageButton runat="server" ID="imgBtnFacebook" ImageUrl="images/Login_with_Facebook-Copy.png"
                OnClick="imgBtnFacebook_Click" meta:resourcekey="imgBtnFacebookResource1" />
            <asp:ImageButton runat="server" ID="imgBtnGoogle" ImageUrl="images/Login_with_Google.png"
                OnCommand="OpenLogin_Click" CommandArgument="https://www.google.com/accounts/o8/id"
                CssClass="sfGoogle" meta:resourcekey="imgBtnGoogleResource1" style="display:none"/>
            <asp:ImageButton runat="server" ID="imgBtnYahoo" ImageUrl="images/Login_with_Yahoo.png"
                OnCommand="OpenLogin_Click" CommandArgument="https://me.yahoo.com" meta:resourcekey="imgBtnYahooResource1" style="display:none"/>
            <asp:ImageButton runat="server" ID="imgBtnLinkedIn" ImageUrl="images/Login_with_LinkedIn.png"
                OnClick="imgBtnLinkedIn_Click" meta:resourcekey="imgBtnLinkedInResource1" style="display:none"/>
            <asp:Label ID="lblAlertMsg" runat="server" Text="Login" Visible="False" meta:resourcekey="lblAlertMsgResource1"></asp:Label>
            <span class="sfOr sfLocale">or</span>
        </div>
        </div>
       
    </asp:View>
    <asp:View ID="View2" runat="server">
        <uc1:LoginStatus ID="LoginStatus1" runat="server" />
    </asp:View>
</asp:MultiView>
