﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ctl_UserRegistration.ascx.cs"
    Inherits="SageFrame.Modules.UserRegistration.ctl_UserRegistration" %>

<script type="text/javascript">
    //<![CDATA[
    $(document).ready(function () {
        $('#<%=FirstName.ClientID%>').keypress(function (e) {
            var regex = new RegExp("^[a-zA-Z]+$");
            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            if (regex.test(str)) {
                return true;
            }
            else {
                e.preventDefault();
                return false;
            }
        });
        $('#<%=LastName.ClientID%>').keypress(function (e) {
            var regex = new RegExp("^[a-zA-Z]+$");
            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            if (regex.test(str)) {
                return true;
            }
            else {
                e.preventDefault();
                return false;
            }
        });
        validate();
        $('input').on('keyup',function () {
            validate();
        });
        $('select').on('change', function () {
            validate();
        });
    });
    function validate() {
            var varpwdID = $("#<%=Password.ClientID%>").val();
            var varcap = $("#<%=CaptchaValue.ClientID%>").val();
            var varfname = $("#<%=FirstName.ClientID%>").val(); 
            var varlname = $("#<%=LastName.ClientID%>").val();
            var varemail = $("#<%=Email.ClientID%>").val();
            var varmobileno =$("#<%=MobileNo.ClientID%>").val();
            var varcityname = $("#<%=ddlCity.ClientID%>").val();
            var varcpwdID = $("#<%=ConfirmPassword.ClientID%>").val();
            if (varpwdID == "" || varcap == "" || varfname == "" || varlname == "" ||  varemail == "" || varmobileno == "" || varcityname == "" || varcpwdID=="") {
                $("input[type=submit]").prop("disabled", true);
                $(".sfBtn").addClass("notallowed");

  } else {
                $("input[type=submit]").prop("disabled", false);
                $(".sfBtn").removeClass("notallowed");
  }
}
    $(document).ready(function () {
        $("#form1").validate({
            ignore: ':hidden',
            rules: {
                '<%=Email.UniqueID %>': { email: true },
                '<%=MobileNo.UniqueID %>': { phone: true }

            },
            messages: {
                '<%=Email.UniqueID %>': "Email must be in a correct format.",
                '<%=MobileNo.UniqueID %>': "Please give Valid Mobile No."

            }
        });
        $(".sfLocalee").SystemLocalize();
        var FinishButton = '#' + '<%=FinishButton.ClientID %>';
        var pwdID = '#' + '<%=Password.ClientID%>';
        var cap = '#' + '<%=CaptchaValue.ClientID%>';
        var fname = '#' + '<%=FirstName.ClientID%>'; 
        var lname = '#' + '<%=LastName.ClientID%>';
        var email = '#' + '<%=Email.ClientID%>';
        var mobileno = '#' + '<%=MobileNo.ClientID%>';
        var cityname = '#' + '<%=ddlCity.ClientID%>';
        var cpwdID = '#' + '<%=ConfirmPassword.ClientID%>';
        $('#minchar').remove();
        $(pwdID).val('');
        $(pwdID).on("change", function () {
            var len = $(this).val().length;
            if (len < 6 && len != 0) {
                $(this).after('<label class="sfError" id="lblPassswordLength"><br/>Password must be at least 6 chars long</label>');
                return false;
            }
            else {
                $('#lblPassswordLength').remove();
            }
        });
        $(pwdID).click(function () {
            $('#lblPassswordLength').remove();
        });
        $(FinishButton).click(function () {
            if ($(cityname).val() == "" || $(mobileno).val() == "" ||  $(lname).val() == "" || $(uname).val() == "" || $(email).val() == "" || $(pwdID).val() == "" || $(cpwdID).val() == "" || $(cap).val() == "")
            {
               csscody.alert("<h1>Registration Failed</h1><p>Please fill all valid fields.</p>")
            }
            else if ($(email).val() != "" || $(email).val() != null)
            {
                var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if (!filter.test($(email).val())) {
                    csscody.alert("<h1>Alert Message</h1><p>Please provide valid email address.</p>")
                    email.focus;
                    return false;
                }
                var len = $(pwdID).val().length;
                if (len < 6) {
                    return false;
                }
                else {
                   // csscody.alert("<h1>Successful Message</h1><p>Registration Successful.</p>")   //Spell Corrected :Successful
                }
            }      
        });

        $('.password').pstrength({ minchar: 6 });

    });
    function pageLoad(sender, args) {
        if (args.get_isPartialLoad()) {
            $('.password').pstrength({ minchar: 6 });
        }
    }
    function isAlfa1(evt) {
        //debugger;
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        var ret = ((charCode >= 65 && charCode <= 90) || (charCode >= 97 && charCode <= 122) || (charCode == 32 || charCode == 0) || (charCode == 8 || charCode == 46));
        if (!ret) {
            $('#lblmessage1').text(GetSystemLocale("Please Enter Valid first Name")).css({ "visibility": "visible", "display": "block" });
            return ret;
        }
        else
        $('#lblmessage1').text(GetSystemLocale("Please Enter Valid first Name")).css({ "visibility": "visible", "display": "none" });
    }
    function isAlfa2(evt) {
        //debugger;
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        var ret = ((charCode >= 65 && charCode <= 90) || (charCode >= 97 && charCode <= 122) || (charCode == 32 || charCode == 0) || (charCode == 8 || charCode == 46));
        if (!ret) {
            $('#lblmessage2').text(GetSystemLocale("Please Enter Valid Last Name")).css({ "visibility": "visible",  "display": "block" });
            return ret;
        }
        else
        $('#lblmessage2').text(GetSystemLocale("Please Enter Valid Last Name")).css({ "visibility": "visible",  "display": "none" });
    }
    //added by jyoti
    function validateLimit(obj, divID, maxchar) {
       objDiv = get_object(divID);
        if (this.id) obj = this;
        var remaningChar = maxchar - trimEnter(obj.value).length;
        if (remaningChar <= 0) {
            obj.value = obj.value.substring(maxchar, 0);
            return false;
        }
        else { return true; }
    }
    function get_object(id) {
        var object = null;
        if (document.layers) {
            object = document.layers[id];
        } else if (document.all) {
            object = document.all[id];
        } else if (document.getElementById) {
            object = document.getElementById(id);
        }
        return object;
    }
    function trimEnter(dataStr) {
        return dataStr.replace(/(\r\n|\r|\n)/g, "");
    }
    //]]>	
</script>


<div class="sfUserRegistrationPage">
    <div class="sfUserRegistration">
        <h2>Sign Up</h2>
        <div class="sfFormwrapper">
            <div class="sfUserRegistrationInfoLeft" id="divRegister" runat="server">
            <div style="width: 100%">
                    <div>
                      <span class="sfAllrequired sfLocalee">* All Fields are compulsory. </span>
                    </div>
                    <div>
                        <div class="cssRegisLabel">
                             <asp:Label ID="FirstNameLabel" runat="server" AssociatedControlID="FirstName" CssClass="sfFormlabel"
                                meta:resourcekey="FirstNameLabelResource1">First Name: </asp:Label>
                        </div>
                        <div class="cssRegisText">
                             <asp:TextBox ID="FirstName" CssClass="sfInputbox" autofocus="autofocus" runat="server"
                                meta:resourcekey="FirstNameResource1" MaxLength="50" ></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvFirstName" runat="server" ControlToValidate="FirstName"
                                Display="Dynamic" ErrorMessage="*" ValidationGroup="CreateUserWizard1" CssClass="sfError"
                                meta:resourcekey="rfvFirstNameResource1"></asp:RequiredFieldValidator>
                             <label id="lblmessage1" style="font-weight: 100;/*text-align: center;*/padding-left: 5px;display:none" class="sfError sfLocale">text</label>
                        </div>
                        <div class="cssRegisLabel">
                             <asp:Label ID="LastNameLabel" runat="server" AssociatedControlID="LastName" CssClass="sfFormlabel"
                                meta:resourcekey="LastNameLabelResource1">Last Name:</asp:Label>
                        </div>
                        <div class="cssRegisText">
                         <asp:TextBox ID="LastName" CssClass="sfInputbox" runat="server" meta:resourcekey="LastNameResource1" MaxLength="50" ></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvLastName" runat="server" ControlToValidate="LastName"
                                Display="Dynamic" ErrorMessage="*" ValidationGroup="CreateUserWizard1" CssClass="sfError"
                                meta:resourcekey="rfvLastNameResource1"></asp:RequiredFieldValidator>
                             <label id="lblmessage2" style="font-weight: 100;/*text-align: center;*/padding-left: 5px;display:none" class="sfError sfLocale">text</label>
                        </div>
                    </div>
                    <div>
                      
                        <div class="cssRegisLabel">
                             <asp:Label ID="EmailLabel" runat="server" AssociatedControlID="Email" CssClass="sfFormlabel"
                                meta:resourcekey="EmailLabelResource1">E-mail:</asp:Label>
                        </div>
                        <div class="cssRegisText">
                             <asp:TextBox ID="Email" MaxLength="50" runat="server" CssClass="sfInputbox" meta:resourcekey="EmailResource1"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvEmailRequired" runat="server" ControlToValidate="Email"
                                Display="Dynamic" ErrorMessage="*" ValidationGroup="CreateUserWizard1" CssClass="sfError"
                                meta:resourcekey="rfvEmailRequiredResource1"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="Email"
                                Display="Dynamic" SetFocusOnError="True" ErrorMessage="*" ValidationGroup="CreateUserWizard1"
                                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" CssClass="sfError1"
                                meta:resourcekey="revEmailResource1"></asp:RegularExpressionValidator>
                             <label id="lblmessage4" style="font-weight: 100;/*text-align: center;*/padding-left: 5px;display:none" class="sfError sfLocale">text</label>
                        </div>
                        <%-- added by jyoti for mob no.--%>
                         <div class="cssRegisLabel">
                          <asp:Label ID="MobileNoLabel" runat="server" AssociatedControlID="MobileNo" CssClass="sfFormlabel"
                                meta:resourcekey="MobileNumberLabelResource1">Mobile :</asp:Label>
                        </div>
                        <div class="cssRegisText">
                            <asp:TextBox ID="MobileNo" MaxLength="10" runat="server" TextMode="Number" CssClass="sfInputbox" meta:resourcekey="MobileNumberResource1" onkeypress="return validateLimit(this, 'lblMsg2', 10)" ></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvMobileNoRequired" runat="server" ControlToValidate="MobileNo" 
                                Display="Dynamic" ErrorMessage="*" ValidationGroup="CreateUserWizard1" CssClass="sfError"
                                meta:resourcekey="rfvMobileNoRequiredResource1" ></asp:RequiredFieldValidator>
                             <label id="lblmessage5" style="font-weight: 100;/*text-align: center;*/padding-left: 5px;display:none" class="sfError sfLocale">text</label>
                        </div>
                       <%-- added by jyoti for city selection.--%>

                          <div class="cssRegisLabel">
                          <asp:Label ID="CityNameSelectionsLabel" runat="server"  CssClass="sfFormlabel"
                                meta:resourcekey="CityNameSelectionsLabelResource1" >City :</asp:Label>
                        </div>
                         <div class="cssRegisText">
                        <asp:DropDownList ID="ddlCity" runat="server" CssClass="sfInputbox">
                                    <asp:ListItem Value="" meta:resourcekey="ListItemResource1">-- Select City --</asp:ListItem>
                        </asp:DropDownList>
                             </div>
                  <%--ended--%>
                    </div>
      <div>
                         <div class="cssRegisLabel">
                         <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password" CssClass="sfFormlabel"
                                meta:resourcekey="PasswordLabelResource1">Password:</asp:Label>
                        </div>
                        <div class="cssRegisText">
                            <asp:TextBox ID="Password" MaxLength="20" runat="server" TextMode="Password" CssClass="sfInputbox password"
                                meta:resourcekey="PasswordResource1"></asp:TextBox>

                            <asp:RequiredFieldValidator ID="rfvPasswordRequired" runat="server" ControlToValidate="Password"
                                Display="Dynamic" ErrorMessage="*" ValidationGroup="CreateUserWizard1" CssClass="sfError"
                                meta:resourcekey="rfvPasswordRequiredResource1"></asp:RequiredFieldValidator>
                        </div>
                        <div class="cssRegisLabel">
                              <asp:Label ID="ConfirmPasswordLabel" runat="server" AssociatedControlID="ConfirmPassword"
                                CssClass="sfFormlabel" meta:resourcekey="ConfirmPasswordLabelResource1">Confirm Password:</asp:Label>
                        </div>
                        <div class="cssRegisText">
                             <asp:TextBox ID="ConfirmPassword" runat="server" TextMode="Password" MaxLength="20"
                                CssClass="sfInputbox" meta:resourcekey="ConfirmPasswordResource1"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvConfirmPasswordRequired" runat="server" ControlToValidate="ConfirmPassword"
                                Display="Dynamic" ErrorMessage="*" ValidationGroup="CreateUserWizard1" CssClass="sfError"
                                meta:resourcekey="rfvConfirmPasswordRequiredResource1"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="cvPasswordCompare" runat="server" ControlToCompare="Password"
                                ControlToValidate="ConfirmPassword" Display="Dynamic" ErrorMessage="*" ValidationGroup="CreateUserWizard1"
                                CssClass="sfError" meta:resourcekey="cvPasswordCompareResource1"></asp:CompareValidator>
                        </div>
                    </div>
       <div  style="width:100%;float:left;padding-top:5px">
                    <div runat="server" id="captchaTR" class="cssHalf">
                        <div class="cssHalfLabel">
                            <asp:Label ID="CaptchaLabel" runat="server" Text="Captcha:" AssociatedControlID="CaptchaImage"
                                CssClass="sfFormlabel" meta:resourcekey="CaptchaLabelResource1"></asp:Label>
                        </div>
                        <div class="sfCatpchatd cssHalfText sfCaptchaImage">
                            <asp:Image ID="CaptchaImage" runat="server" CssClass="sfCaptcha" meta:resourcekey="CaptchaImageResource1" />
                            <span id="captchaValidator" runat="server" class="sfrequired">*</span>
                            <asp:ImageButton ID="Refresh" CssClass="sfCaptchadata" runat="server" OnClick="Refresh_Click"
                                ValidationGroup="Sep" meta:resourcekey="RefreshResource1" />
                        </div>
                    </div>
                    <div class="cssHalf">
                        <div class="cssHalfLabel">
                            <asp:Label ID="DataLabel" runat="server" Text="Enter Captcha Text" AssociatedControlID="CaptchaValue"
                                CssClass="sfFormlabel" meta:resourcekey="DataLabelResource1"></asp:Label>
                        </div>
                        <div class="cssHalfText sametext" style="margin-bottom:5%">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                      <asp:TextBox ID="CaptchaValue" runat="server" CssClass="sfInputbox" meta:resourcekey="CaptchaValueResource1" MaxLength="6"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvCaptchaValueValidator" runat="server" ControlToValidate="CaptchaValue"
                                        Display="Dynamic" ErrorMessage="*" ValidationGroup="CreateUserWizard1" CssClass="sfError"
                                        meta:resourcekey="rfvCaptchaValueValidatorResource1"></asp:RequiredFieldValidator>
                                   
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                         
                    </div>
                   </div>
                <div class="sfButtonwrapper">
                                <asp:Button ID="FinishButton" runat="server" AlternateText="Finish" ValidationGroup="CreateUserWizard1"
                                    CommandName="MoveComplete" CssClass="sfBtn" Text="Sign Up" OnClick="FinishButton_Click"
                                    meta:resourcekey="FinishButtonResource1" disabled="true"/>
                            </div>  
               <div class="term_con"> <p >By signing up, you agree to our  <a target="_blank" href="/TermsCondition">Terms and conditions.</a></p></div> 
        </div>
            </div>
            <div id="divRegistration" runat="server">
                <div class="sfRegistrationInformation">
                    <%= headerTemplate %>
                </div>
                <div id="divRegConfirm" class="sfRegConfirm" runat="server">
                    <h3>Registration Successful</h3>
                    <asp:Label ID="lblRegSuccess" runat="server" CssClass="sfFormlabel" meta:resourcekey="lblRegSuccessResource1">  </asp:Label>
                    <asp:Literal ID="USER_RESISTER_SUCESSFUL_INFORMATION" runat="server" meta:resourcekey="USER_RESISTER_SUCESSFUL_INFORMATIONResource1"></asp:Literal>
                    <div class="sfButtonwrapper">
                        <span><a href='<%=LoginPath%>' class="sfBtn">Go To Login Page</a></span>
                    </div>
                </div>
            </div>
           <!--
        <asp:CheckBox ID="chkIsSubscribeNewsLetter" runat="server" CssClass="sfCheckbox" />
        <asp:Label ID="lblIsSubscribeNewsLetter" runat="server" Text="Subscribe Newsletter:"
                AssociatedControlID="CaptchaValue" CssClass="sfFormlabel"></asp:Label>
        <br />
        -->
        </div>
    </div>
</div>
