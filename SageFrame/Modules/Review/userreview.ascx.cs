﻿using SageFrame.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using SanchiCommerce.Core;
using SageFrame.Core;
using SageFrame.Common;
using System.Text;
using SageFrame.Localization;
using SageFrame;
using DocumentFormat.OpenXml.EMMA;
using System.Collections;

public partial class Modules_Review_userreview : BaseUserControl
{
   
    private int storeID,
               portalID,
               customerID;
    private string userName, cultureName;
    private string sessionCode = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        SageFrameConfig pagebase = new SageFrameConfig();
        SageFrameRoute parentPage = (SageFrameRoute)this.Page;
      
        string templateName = TemplateName;
        GetPortalCommonInfo(out storeID, out portalID, out customerID, out userName, out cultureName, out sessionCode);
        AspxCommonInfo aspxCommonObj = new AspxCommonInfo(storeID, portalID, userName, cultureName, customerID, sessionCode);

        if (!IsPostBack)
        {
            // this.serviceURL = ResolveUrl(this.AppRelativeTemplateSourceDirectory) + "Services/WebService.asmx/";
            IncludeCss("Review", "/Templates/" + TemplateName + "/css/PopUp/style.css",
                       "/Templates/" + TemplateName + "/css/StarRating/jquery.rating.css",
                       "/Templates/" + TemplateName + "/css/JQueryUIFront/jquery-ui.all.css",
                       "/Templates/" + TemplateName + "/css/MessageBox/style.css",
                       "/Templates/" + TemplateName + "/css/FancyDropDown/fancy.css",
                       "/Templates/" + TemplateName + "/css/ToolTip/tooltip.css",
                       "/Templates/" + TemplateName + "/css/PopUp/popbox.css", "Modules/Review/css/module.css", "/Templates/SanchiCommerce/css/template.css", "/css/toaster.css");
            IncludeJs("MenuManager", false, "/Administrator/Templates/Default/js/ajaxupload.js","/js/StarRating/jquery.MetaData.js", "/js/jquery.validate.js");
             IncludeJs("Review", "Modules/Review/js/Review.js","Modules/Review/Language/AspxUserReviewDetails.js","/js/toaster.min.js");
        }
        IncludeLanguageJS();
        GetReviewForm(aspxCommonObj);
    }
    private Hashtable hst = null;
    private string getLocale(string messageKey)
    {
        string retStr = messageKey;
        if (hst != null && hst[messageKey] != null)
        {
            retStr = hst[messageKey].ToString();
        }
        return retStr;
    }
    public void GetReviewForm(AspxCommonInfo aspxCommonObj)
    {

     
        string resolvedUrl = ResolveUrl("~/");
        string modulePath = this.AppRelativeTemplateSourceDirectory;
        string aspxTemplateFolderPath = resolvedUrl + "Templates/" + TemplateName;
        string aspxRootPath = resolvedUrl;
        hst = AppLocalized.getLocale(modulePath);
        string pageExtension = SageFrameSettingKeys.PageExtension;
        List<GroupInfo> arrList = new List<GroupInfo>();
        StringBuilder itemTagsBody = new StringBuilder();
        StringBuilder dynHtml = new StringBuilder();
        dynHtml.Append("<div id=\"dynReviewDetailsForm\" class=\"sfFormwrapper\" style=\"display:none\">");
        dynHtml.Append("<div id=\"Review_TabContainer\" class=\"responsive-tabs\">");
        if (aspxCommonObj.CustomerID > 0 && aspxCommonObj.UserName.ToLower() != "anonymoususer")
        {
            itemTagsBody.Append("<div id=\"ReviewForm\"><a  onclick=\"pop1(");
            itemTagsBody.Append(")\">");
            itemTagsBody.Append("<h2>Rate & Review </h2>");
            itemTagsBody.Append("</a></div>");
        }
        else
        {
            SageFrameConfig sfConfig = new SageFrameConfig();
            itemTagsBody.Append("<div id=\"ReviewF\">");
            itemTagsBody.Append("<h2><a onclick=\"pop(");
            itemTagsBody.Append(")\">Rate & Review ");
            itemTagsBody.Append("</a></h2></div>");

            itemTagsBody.Append("<a href=\"");
            itemTagsBody.Append(aspxRedirectPath);
            itemTagsBody.Append(sfConfig.GetSettingsByKey(SageFrameSettingKeys.PortalLoginpage));
            itemTagsBody.Append(pageExtension);
            itemTagsBody.Append("?ReturnUrl=");
            itemTagsBody.Append(aspxRedirectPath);
            itemTagsBody.Append("\" class=\"cssClassLogIn\"><span>");
            //itemTagsBody.Append(getLocale("Enter Your Review"));
            itemTagsBody.Append("</span></a>");
        }
        dynHtml.Append(itemTagsBody);
        dynHtml.Append("</div></div>");
        ltrReviewDetailsForm.Text= dynHtml.ToString();
    }
    }