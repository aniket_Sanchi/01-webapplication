﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="CategoryWiseItemsSettings.ascx.cs" Inherits="Modules_AspxCommerce_AspxCategoryWiseItemsList_CategoryWiseItemsSettings" %>

<div class="classCategoryItemsSettings">
    <h2 class="sfLocale">Items In Category Settings</h2>
    <table>
        <tr><td><label id="lblNoOfItemsInCategory" class="sfLocale">No Of Items In Category</label></td><td><input type="text" id="txtNoOfItems"/></td></tr>
    </table>
    <input type="button" id="btnSave" class="sfLocale" value="Save"/>
</div>

<script type="text/javascript">
(function($) {
    $.CategoryWiseItemSettingView = function(p) {
        p = $.extend
        ({
           catWiseItemModulePath: '',
            noOfItemsInCategory: 0
        }, p);
        var aspxCommonObj = function() {
            var aspxCommonInfo = {
                StoreID: AspxCommerce.utils.GetStoreID(),
                PortalID: AspxCommerce.utils.GetPortalID(),               
                CultureName: AspxCommerce.utils.GetCultureName()
            };
            return aspxCommonInfo;
        };
        var categoryWiseItemSettings = {
            config: {
                isPostBack: false,
                async: true,
                cache: false,
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                data: '{}',
                dataType: 'json',
                baseURL: p.catWiseItemModulePath,                
                method: "",
                url: "",
                ajaxCallMode: 0,
                itemid: 0
            },
            ajaxCall: function(config) {
                $.ajax({
                    type: categoryWiseItemSettings.config.type,
                    contentType: categoryWiseItemSettings.config.contentType,
                    cache: categoryWiseItemSettings.config.cache,
                    async: categoryWiseItemSettings.config.async,
                    url: categoryWiseItemSettings.config.url,
                    data: categoryWiseItemSettings.config.data,
                    dataType: categoryWiseItemSettings.config.dataType,
                    success: categoryWiseItemSettings.config.ajaxCallMode,
                    error: categoryWiseItemSettings.config.ajaxFailure
                });
            },
            SaveAndUpdateSettings: function(noOfItemsInCategory) {
                this.config.method = "Services/CategoryWiseItemListHandler.ashx/SaveCategoryItemSettings";
                this.config.url = p.catWiseItemModulePath + this.config.method;
                this.config.data = JSON2.stringify({ noOfItemInCategory: noOfItemsInCategory, aspxCommonObj: aspxCommonObj() });
                this.config.ajaxCallMode = categoryWiseItemSettings.SaveSuccessfull;
                this.ajaxCall(this.config);
            },
            BindSetting: function(data) {
                $("#txtNoOfItems").val(p.noOfItemsInCategory);
            },
            SaveSuccessfull: function() {
                SageFrame.messaging.show(getLocale(AspxCategoryWiseItem, "Setting Saved Successfully"), "Success");
            },
            init: function() {
                categoryWiseItemSettings.BindSetting();
                $("#btnSave").click(function() {
                    var noOfItemsInCategory = $("#txtNoOfItems").val();
                    categoryWiseItemSettings.SaveAndUpdateSettings(noOfItemsInCategory);
                });
            }
        };
        categoryWiseItemSettings.init();
        };
        $.fn.CategoryWiseItemSetting = function(p) {
        $.CategoryWiseItemSettingView(p);
        };
})(jQuery);
$(function() {
    $(".sfLocale").localize({
        moduleKey: AspxCategoryWiseItem
    });
    $(this).CategoryWiseItemSetting({
        catWiseItemModulePath: '<%=CatWiseItemModulePath %>',
        noOfItemsInCategory: '<%=NoOfItemsInCategory %>'
    });
});
</script>
