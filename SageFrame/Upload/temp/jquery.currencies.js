
/*
* Currency tools
*
* Copyright (c) 2013 Caroline Schnapp (mllegeorgesand@gmail.com)
* Licensed under the MIT license:
* http://www.opensource.org/licenses/mit-license.php
*
*/

if (typeof Currency === 'undefined') {
    var Currency = {};
}
var Currency = {
    rates: {},
    convert: function (amount, from, to) {
        return (amount * Currency.rates[to]) / Currency.rates[from];
    }
};

getRate = function () {
    if (currencyRate != undefined) {
        var rate = $.parseJSON(currencyRate);
        $.each(rate, function (index, item) {
            Currency.rates[item.CurrencyCode] = item.CurrencyRate;
        });
    }
}();
Currency.cookie = {
    configuration: {
        expires: 1,
        path: '/',
        domain: window.location.hostname
    },
    name: 'currency' + SageFramePortalID + '_' + SageFramePortalID,
    write: function (currency) {
        this.destroy();
        var d = new Date();
        d.setTime(d.getTime() + (this.configuration.expires * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toGMTString();
        var domain = "domain=" + this.configuration.domain;
        var path = "path=" + this.configuration.path;
        document.cookie = this.name + "=" + currency + "; " + expires + ";" + path;
    },
    read: function () {
        var name = this.name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i].trim();
            if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
        }
        return "";
    },
    destroy: function () {
        document.cookie = this.name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    }
};

Currency.money_with_currency_format = {
    "USD": "${{amount}} USD",
    "EUR": "&euro;{{amount}} EUR",
    "GBP": "&pound;{{amount}} GBP",
    "CAD": "${{amount}} CAD",
    "ARS": "${{amount_with_comma_separator}} ARS",
    "AUD": "${{amount}} AUD",
    "BBD": "${{amount}} Bds",
    "BDT": "Tk {{amount}} BDT",
    "BSD": "BS${{amount}} BSD",
    "BHD": "{{amount}}0 BHD",
    "BRL": "R$ {{amount_with_comma_separator}} BRL",
    "BOB": "Bs{{amount_with_comma_separator}} BOB",
    "BND": "${{amount}} BND",
    "BGN": "{{amount}} лв BGN",
    "MMK": "K{{amount}} MMK",
    "KYD": "${{amount}} KYD",
    "CLP": "${{amount_no_decimals}} CLP",
    "CNY": "￥{{amount}} CNY",
    "COP": "${{amount_with_comma_separator}} COP",
    "CRC": "&#8353; {{amount_with_comma_separator}} CRC",
    "HRK": "{{amount_with_comma_separator}} kn HRK",
    "CZK": "{{amount_with_comma_separator}} K&#269;",
    "DKK": "kr.{{amount_with_comma_separator}}",
    "DOP": "RD$ {{amount_with_comma_separator}}",
    "XCD": "EC${{amount}}",
    "EGP": "LE {{amount}} EGP",
    "XPF": "{{amount_no_decimals_with_comma_separator}} XPF",
    "FJD": "FJ${{amount}}",
    "GHS": "GH&#8373;{{amount}}",
    "GTQ": "{{amount}} GTQ",
    "GYD": "${{amount}} GYD",
    "GEL": "{{amount}} GEL",
    "HKD": "HK${{amount}}",
    "HUF": "{{amount_no_decimals_with_comma_separator}} Ft",
    "ISK": "{{amount_no_decimals}} kr ISK",
