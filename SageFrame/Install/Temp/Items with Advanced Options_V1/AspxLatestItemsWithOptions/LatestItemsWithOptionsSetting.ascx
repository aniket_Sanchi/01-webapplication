﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="LatestItemsWithOptionsSetting.ascx.cs" Inherits="Modules_AspxCommerce_AspxLatestItemsWithOptions_LatestItemsWithOptionsSetting" %>
<div class="cssLatestOptionSetting">
    <h3 class="sfLocale">Latest Item With Option Settings
    </h3>
    <table>
        <tr>
            <td>
                <asp:Label ID="lblLatestItemOptionCount" runat="server"
                    Text="Enter the Number of Items Displayed"></asp:Label>
            </td>
            <td>
                <input type="text" id="txtLatestItemOptionCount" name="LatestItemOptionCount" class="required number" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblShowLatestItemOptionRss" runat="server" Text="Enable Rss"></asp:Label>
            </td>
            <td>
                <input type="checkbox" id="chkEnableLatestItemOptionRss" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblLatestItemOptionRssCount" runat="server"
                    Text="Number of Rss To Show"></asp:Label>
            </td>
            <td>
                <input type="text" id="txtLatestItemOptionRssCount" name="LatestItemOptionRssCount" class="required number" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblLatestItemOptionRssPage" runat="server"
                    Text="Latest Item Carousel Rss Page"></asp:Label>
            </td>
            <td>
                <input type="text" id="txtLatestItemOptionRssPage" disabled="disabled" />
            </td>
        </tr>
        <tr>
            <td>
                <input type="button" id="btnLatestItemOptionSettingSave" class="sfLocale sfbtn" value="Save" />
            </td>
        </tr>
    </table>
</div>
<script type="text/javascript">
    // <![CDATA[
    $(function () {
        $(".sfLocale").localize({
            moduleKey: LatestItemWithOptions
        });
        $(this).LatestItemOptionSetting({
            Settings: '<%=Settings %>'
        });
    });
    // ]]>
</script>