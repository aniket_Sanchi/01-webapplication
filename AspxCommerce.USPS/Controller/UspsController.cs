﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SanchiCommerce.Core;
using SanchiCommerce.USPS.Provider;
using SanchiCommerce.USPS;
using SanchiCommerce.USPS.Entity;
using RegisterModule;
using SageFrame.Web.Utilities;
using System.Web;
using System.Threading;

namespace SanchiCommerce.USPS.Controller
{
   public class UspsController
    {
       public UspsController()
       {

       }

       public UspsSetting GetSetting(int providerId, AspxCommonInfo commonInfo)
       {
           try
           {
               return USPSProvider.GetSetting(providerId, commonInfo);
           }
           catch (Exception ex )
           {
                
               throw ex;
           }
          
       }

    }
}
