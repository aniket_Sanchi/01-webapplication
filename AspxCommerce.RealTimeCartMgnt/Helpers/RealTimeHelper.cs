﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SanchiCommerce.AdminNotification;
using Microsoft.AspNet.SignalR;
using SanchiCommerce.Core;

namespace SanchiCommerce.RealTimeCartManagement
{
    public class RealTimeHelper
    {
        public RealTimeHelper()
        {
        }

        public static void UpdateAdminNotifications(int StoreID, int PortalID)
        {
            try
            {
                AspxCommonInfo aspxCommonObj = new AspxCommonInfo();
                aspxCommonObj.StoreID = StoreID;
                aspxCommonObj.PortalID = PortalID;
                NotificationGetAllInfo listInfo = AdminNotificationController.NotificationGetAll(aspxCommonObj.StoreID, aspxCommonObj.PortalID);
                IHubContext hubContext = GlobalHost.ConnectionManager.GetHubContext<RealTimeHub>();
                hubContext.Clients.All.NotificationGetAllSuccess(listInfo);

            }
            catch (Exception)
            {
                //TODO
            }
        }

    }
}
